<!DOCTYPE html>
<html>
    <style type="text/css">
        #wpisy
        {
	        position: relative;
	        text-align:center;
	    }
	    #calosc
	    {
	        margin: 0 auto;
	        background-color: green;
	    }

	    *.odstep
	    {
	        padding: 10px;
	    }
	    *.na_srodek
	    {
	        position: relative;
	        text-align:center;
	    }
    </style>
    <head>
        <title>mBlog: {{ user[0] }} {{ user[1] }} </title>
    </head>
    <body id="calosc">
        <form action="/~p6/wsgi/blog" method="post">
            <h1 class="odstep">Witaj {{ user[0] }}</h1>
            <div class="odstep"><a href='/~p6/wsgi/'>Wyloguj</a></div><hr />
          <div id="wpisy">
            <p>Twoje wpisy:</p>
            % for i in notes:
                <div> {{ i[1] }}</div>
                <div> {{ i[2] }}</div></br>
            % end
          </div>
          <div>
            <input type="hidden" value="{{user[2]}}" name="login" />
            <input type="hidden" value="{{user[3]}}" name="haslo" />
          </div>
          <div class="na_srodek">
            Dodaj nowy wpis :
          </div>
          <div class="na_srodek">
            <textarea cols="40" rows="7" value="note" name="note"></textarea>
          </div>
          <div class="na_srodek">
            <input type="submit" value="Dodaj" name="submit" />
          </div>
        </form>
    </body>
</html>