# -*- coding: utf-8 -*-
__author__ = 'MadMike'

from bottle import Bottle, run, template, request, error
import sqlite3
import  time

app = Bottle()

@app.route('/')
@app.route('/', method='POST')
def index():
    connection = sqlite3.connect('MadDataBase.db')
    c = connection.cursor()
    allNotes = c.execute("SELECT * FROM Notes")
    return template('index', allNotes=allNotes)

@app.error(404)
def error404(error):
    return template('wrongPath')

@app.route('/login')
def login():
    return template('login')

@app.route('/blog', method='POST')
def blog():
    connection = sqlite3.connect('MadDataBase.db')
    c = connection.cursor()

    login = request.forms.login
    haslo = request.forms.haslo
    note = request.forms.note

    if login != '' and haslo != '':
        # Insert a row of data
        tmp = c.execute("SELECT * FROM Users WHERE login='"+ login +"'")
        user = tmp.fetchone()

        if user is None : return template('loginBAD')

        if note != '':
            localtime = time.asctime( time.localtime(time.time()) )
            tmpNote = c.execute("SELECT * FROM Notes WHERE login='"+ login +"' and note='"+ note +"'")
            if tmpNote.fetchone() is None:
                c.execute("INSERT INTO Notes VALUES ('"+ login +"', '"+ localtime +"', '"+ note +"')")
                connection.commit()

        if user[2] == login and user[3] == haslo:
            notes = c.execute("SELECT * FROM Notes WHERE login='"+ login +"'")
            return template('blog', user=user, notes=notes, note=note)
        else:
            return template('loginBAD')
    else:
        return template('loginBAD')

    connection.close()

@app.route('/register')
def register():
    return template('register')

@app.route('/registercheck', method='POST')
def registercheck():
    imie = request.forms.imie
    nazwisko = request.forms.nazwisko
    login = request.forms.login
    haslo = request.forms.haslo

    if imie != '' and nazwisko != '' and login != '' and haslo != '':
        connection = sqlite3.connect('MadDataBase.db')
        c = connection.cursor()
        # Insert a row of data
        c.execute("INSERT INTO Users VALUES ('"+ imie +"', '"+ nazwisko +"','"+ login +"','"+ haslo +"')")
        # Save (commit) the changes
        connection.commit()
        connection.close()
        return template('registerOK')
    else:
        return template('registerBAD')

@app.route('/users')
def users():
    connection = sqlite3.connect('MadDataBase.db')

    c = connection.cursor()

    users = c.execute("SELECT * FROM Users")

    return template('users_list', ludzie=users)

    connection.close()

@app.route('/jncjkdsfnjksdfnjvknfdkvjnfd/database')
def database():
    connection = sqlite3.connect('MadDataBase.db')

    c = connection.cursor()

    # Create table
    c.execute('''CREATE TABLE Users(name, surname, login, password)''')
    c.execute('''CREATE TABLE Notes(login, date, note)''')

    # Insert a row of data
    c.execute("INSERT INTO Users VALUES ('Michał', 'Wnenk', 'MadMike', 'madmike')")
    c.execute("INSERT INTO Users VALUES ('Adrian', 'Nowy', 'adrian', 'adrian')")
    c.execute("INSERT INTO Users VALUES ('Czacza', 'Blabla', 'czacza', 'czacza')")

    c.execute("INSERT INTO Notes VALUES ('MadMike', 'Sun Apr 13 15:50:21 2014', 'Pierwszy wpis Michała :D')")
    c.execute("INSERT INTO Notes VALUES ('adrian', 'Sun Apr 13 13:30:21 2013', 'Dzisiaj robie placki :D')")
    c.execute("INSERT INTO Notes VALUES ('czacza', 'Sun Apr 13 21:40:21 2012', 'Mam zły humorek :(')")

    # Save (commit) the changes
    connection.commit()
    return 'Baza stworzona i zapisana :)'
    connection.close()

if __name__ == '__main__':
    run(app=app, host="localhost", port=5312)