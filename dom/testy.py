# -*- coding: utf-8 -*-

import unittest
from stronka import app
from webtest import TestApp


class blogTests(unittest.TestCase):

    def setUp(self):
        self.app = TestApp(app)

    def tearDown(self):
        self.app.reset()

    def test_404_Not_Found(self):
        assert self.app.get('/makarenamakarena', status='*').status_int == 404

    def test_200_Stronka_Glowna(self):
        response = self.app.get('/')
        assert response.status_int == 200
        assert 'Witaj na stronce mBlog' in response
        assert 'Logowanie' in response
        assert 'Rejestracja' in response

    def test_200_Stronka_Logowania(self):
        response = self.app.get('/login')
        assert response.status_int == 200
        assert 'Logowanie :' in response
        assert 'Login' in response
        assert 'Hasło' in response

    def test_Logowanie_Na_Zle_Passy(self):
        response = self.app.post('/blog', {'login': 'koks', 'haslo': 'koks'}, status='*')
        assert 'Logowanie nie powiodło się :(' in response
        assert 'Podałeś nieprawidłowy login lub hasło :(' in response
        assert 'Spróbuj zalogować się ponownie' in response

    def test_Logowanie_Na_Dobre_Passy(self):
        response = self.app.post('/blog', {'login': 'adrian', 'haslo': 'adrian'}, status='*')
        assert response.status_int == 200
        assert 'Witaj' in response
        assert 'Wyloguj' in response
        assert 'Twoje wpisy:' in response
        assert 'Dodaj nowy wpis :' in response

    def test_Dodawanie_Postu(self):
        #self.app.post('/blog', {'login': 'adrian', 'haslo': 'adrian'})
        response = self.app.post('/blog', {'login': 'adrian', 'haslo': 'adrian', 'note': 'Test sprawdzający dodawanie posta'}, status='*')
        print response
        assert response.status_int == 200
