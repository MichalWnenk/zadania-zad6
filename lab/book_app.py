# -*- coding: utf-8 -*-

from flask import Flask, render_template
import bookdb

app = Flask(__name__)
app.config['DEBUG'] = True

db = bookdb.BookDB()

@app.route('/')
def books():
    return render_template('book_list.html', tytuly=db.titles())


@app.route('/book/<book_id>/')
def book(book_id):
    if int(book_id[2:]) > 0 and int(book_id[2:]) <= 5:
        info = db.title_info(book_id)
        return render_template('book_detail.html', title=info)
    else:
        return "404"


if __name__ == '__main__':
    app.run('194.29.175.240', 8465, debug=True)