# -*- coding: utf-8 -*-

from flask import Flask
import sqlite3
from contextlib import closing


#################
#  Konfiguracja #
#################

DATABASE = 'microblog.db'
SECRET_KEY = '1234567890!@#$%^&*()'



#############
# Aplikacja #
#############

app = Flask(__name__)
app.config.from_object(__name__)


###############
# Baza danych #
###############

def connect_db():
    return sqlite3.connect(app.config['DATABASE'])


def init_db():
    with closing(connect_db()) as db:
        with app.open_resource('schema.sql') as f:
            db.cursor().executescript(f.read())
        db.commit()



################
# Uruchomienie #
################

if __name__ == '__main__':
    app.run(debug=True)