import sys

# dołączenie do ścieżki wyszukiwania modułów
# katalogu, w którym znajduje się aplikacja
# najlepiej poza ~/www
sys.path.insert(0, '/path/to/application/dir')

# aktywacja środowiska wirtualnego
activate_this = '/path/to/flaskenv/bin/activate_this.py'
execfile(activate_this, dict(__file__=activate_this))

from book_app import app as application